/*
 * @Description: 计算服务器消耗时长的中间件
 * @Author: 梁萌
 * @Date: 2020-11-21 10:41:35
 * @LastEditTime: 2020-11-21 11:01:45
 * @FilePath: \koa_server\middleware\koa_response_duration.js
 */

module.exports = async(ctx, next) => {
    //记录开始时间
    const start = Date.now();
    //让内层中间件得到执行
    await next();
    //记录结束时间
    const end = Date.now();
    //设置响应头 X-Response-Time
    const duration = end - start;
    ctx.set('X-Response-Time', duration + 'ms');
}