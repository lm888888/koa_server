/*
 * @Description: 处理业务逻辑的中间件,读取某个json文件的数据
 * @Author: 梁萌
 * @Date: 2020-11-21 10:40:37
 * @LastEditTime: 2020-11-21 11:48:47
 * @FilePath: \koa_server\middleware\koa_response_data.js
 */
const path = require('path');
const fileUtils = require('../utils/file_utils');

module.exports = async(ctx, next) => {
    const url = ctx.request.url; //请求路径
    let filePath = url.replace('/api', '');
    filePath = '../data' + filePath + '.json'; //文件路径
    filePath = path.join(__dirname, filePath);

    try {
        const jsonData = await fileUtils.getFileJsonData(filePath); //根据文件路径获取文件内容
        ctx.response.body = jsonData; //将json数据放入相应体中
    } catch (error) {
        const errorMsg = {
            message: '读取文件内容失败,文件资源不存在',
            status: 404
        }
        ctx.response.body = JSON.stringify(errorMsg);
    }

    await next();
}