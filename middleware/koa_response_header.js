/*
 * @Description: 设置响应头的中间件
 * @Author: 梁萌
 * @Date: 2020-11-21 10:42:23
 * @LastEditTime: 2020-11-21 12:05:18
 * @FilePath: \koa_server\middleware\koa_response_header.js
 */

module.exports = async(ctx, next) => {
    const contentType = 'application/json; charset=utf-8';
    ctx.set('Content-Type', contentType);
    await next();
}