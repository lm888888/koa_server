/*
 * @Description: 服务器入口文件
 * @Author: 梁萌
 * @Date: 2020-11-21 10:36:14
 * @LastEditTime: 2020-11-21 12:08:19
 * @FilePath: \koa_server\app.js
 */

//1.创建koa的实例对象
const Koa = require('koa');
const cors = require('koa-cors'); //跨域配置
const app = new Koa();

//2.绑定中间件
//绑定第一层中间件
const resDurationMiddleware = require('./middleware/koa_response_duration');
app.use(resDurationMiddleware);
//绑定第二层中间件
const resHeaderMiddleware = require('./middleware/koa_response_header');
app.use(resHeaderMiddleware);
//绑定第三层中间件
const resDataMiddleware = require('./middleware/koa_response_data');
app.use(resDataMiddleware);
//3.绑定端口号
app.listen(3000);

//4.允许跨域请求
app.use(cors());