/*
 * @Description: 读取文件的工具方法
 * @Author: 梁萌
 * @Date: 2020-11-21 10:42:56
 * @LastEditTime: 2020-11-21 11:43:07
 * @FilePath: \koa_server\utils\file_utils.js
 */
const fs = require('fs');
module.exports.getFileJsonData = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (error, data) => {
            if (error) {
                //读取文件失败
                reject(error);
            } else {
                //读取文件成功
                resolve(data);
            }
        });
    });
}